call insert_book (
'9780133432381', -- isbn_13
'0133432386', -- isbn_10
'C Primer Plus', -- title
'https://www.goodreads.com/book/show/23197882-c-primer-plus', -- url
'https://images.gr-assets.com/books/1410327746l/23197882.jpg', -- cover
'2013-11-25', -- publish date
6, -- edition
1, -- volume
null, -- publisher id
'Addison-Wesley Professional', -- publisher name
null, -- authors group id
'Stephen', -- author's first name
'Prata', -- author's last name
1300000, -- price
1, -- unit id
null, -- unit name
4, -- shelf row number
2, -- shelf column number
'United States', -- country
1060, -- number of pages
null, -- translators group id
null, -- translator's first name
null, -- translator's last name
1, -- language id
null, -- language name
1, -- topic id
null, -- topic name
null, -- user id
'Brian', -- user's first name
'Salehi', -- user's last name
false, -- has borrowed
false, -- has lent
1, -- state id
null); -- rate

call insert_book (
'9780321776402', -- isbn_13
'0321776402', -- isbn_10
'C++ Primer Plus', -- title
'https://www.amazon.com/Primer-Plus-6th-Developers-Library/dp/0321776402', -- url
'https://images.gr-assets.com/books/1521853574l/39358670.jpg', -- cover
'2011-10-28', -- publish date
6, -- edition
2, -- volumes
1, -- publisher id
null, -- publisher name
1, -- authors group id
null, -- author's first name
null, -- author's last name
1450000, -- price
1, -- unit id
null, -- unit name
4, -- shelf row number
2, -- shelf column number
'United States', -- country
1440, -- number of pages
null, -- translators group id
null, -- translator's first name
null, -- translator's last name
1, -- language id
null, -- language name
1, -- topic id
null, -- topic name
1, -- user id
null, -- user's first name
null, -- user's last name
false, -- has borrowed
false, -- has lent
3, -- state id
5); -- rate
