# Ideas & Structures

## Pure Ideas

* none

## ToDos

* add cover type in books

## Main Functionality

x user can insert a book in a shelf
* user can move a book from one shelf to another
* user can throw a book away from library
* user can start reading a book
* user can stop reading a book
* user can finish reading a book
* user can choose a book to read later
* user can rate a book that finished reading
x users can read the list of their books in the library, within book states
x user can look for a book in library based on different criteria
* user can lend books
* user can borrow books
* user can share books information
* user can sort books in shelves
* user can add shelves to library
* user can remove shelves from library
* user can pull off (clean up) the shelves
* user can write books
* user can move books from a shelf to another

## Functions Based on Functionalities

### Insertion

+ back-end will get ISBN and looks for book's information if exists,
if not, prompts user to give them
+ procedure will get necessary and additional information in parameters and inserts them into library
+ estimate the capacity of a shelf based on filled shelves above and below it (fun!)
+ if cell coordinates were not specified, suggest them!
+ check if a shelf is full for suggestions
+ if new topic is mentioned, it will be created by calling its procedure
* suggest shelves with similar topic to put books in, consider spaces not belonging to lent books,
specifically suggest the shelf, if any, which was previously holding that book.
+ change shelf's topic based on most abundant holding topics right after insetion
+ increment number-of-holding-books of that shelf
* increment usage-frequency of that shelf right after insertion
if the book have already been held in the library before the user took it
+ if new translator is mentioned, it would be created by calling the proper procedure
+ if new currency unit is given and price exists, it would be created by calling the proper procedure
+ if new authors is mentioned, it would be created by calling the proper procedure
+ if new publisher is given, it would be created by calling the proper procedure
+ book records are repeated for each number-of-volumes, each record holding one volume number
+ if input user's name does not exist, book will be inserted
but will not be owned by anyone until username is set separately

### Movement

+ decrement number-of-holding-books from old shelf and increment for new one
+ change shelf's topic based on most abundant holding topics right after removal from old shelf and insertion into new one
* suggest new shelves that can hold the books which user wants to move from old shelf,
make special suggestions for shelves with similar topic

### Removal

* set cellid to null on book's record
* remove user activities for that book
* decrement number-of-holding-books of shelf
* change shelf's topic based on most abundant holding topics right after removal

### Currently Reading State

* create record in activities for that user, if user does not exist, ignore

### Stop Reading State

* change the state to left unfinished

### Finished Reading State

* change the state to read
* set user rate

###  Want to Read State

* change the state to want to read

### List of Books

* create view to show the list

### Find Book

* query based on isbn-10, isbn-13, title, publisher, author, translator

### Lend Book

* decrement shelf's number-of-holding-books
* set has-lent flag in activities

### Borrow Book

* increment holding shelf's number-of-holding-books number
* set has-borrowed flag in activities

### Share Book Information

* make a view to gather book's information and return as a message

### Sort Books in a Shelf

* suggest the order based on a-z names, reverse, date inserted into shelf, and all possible criteria

### move shelf

* switch cell numbers from two changing shelves

## Elements

### json (autofill)

title
publisher-name
author-name
isbn-10
isbn-13
url
number-of-pages
cover
publish-date

### database (manual)

edition
volume (user gives number of volumes, records will be repeated for each volume)
price
country
translators
language
section

## Functions Based on Tables

### topics

* user can add, remove or edit topics

### shelves

* user can add, remove or edit shelves anywhere between shelves (good luck!)

### languages

* user can add, remove or edit languages

### translators

* user can do all operations on translators
* by adding each translator in this table, one record should be created in translators-group table too

### translators-groups

* none

### currency-units

* user can do all operations on translators

### price-list

* user can do all operations on book prices

### authors

* by adding each author, one record should be created in authors-group too

### authors-group

* none

### publishers

* user can do all operations on publishers record

### books

* user can add, edit and delete a book on books table

### users

* user can add, edit or delete on users records

### activities

* user can add a book as currently reading, read or want to read
* user can rate a book from 1 to 5 after reading a book
* user can set a book as borrowed or has-lent, but not both!
