create database library;
use library;

create table topics (
    id int primary key auto_increment,
    name varchar(40) not null
);

create table shelves (
    id int primary key auto_increment,
    row_number int not null,
    column_number int not null,
    usage_frequency int not null default 0,
    capacity int not null default 5,
    number_of_holding_books int not null default 0,
    topic_id int,
    foreign key (topic_id) references topics (id) on update cascade on delete set null
);

create table languages (
    id int primary key auto_increment,
    name varchar(20) not null
);

create table translators (
    id int primary key auto_increment,
    first_name varchar(40) default null,
    last_name varchar(40) not null,
    email varchar(60) default null,
    language_id int,
    foreign key (language_id) references languages (id) on update cascade on delete set null
);

create table translators_group (
    id int primary key auto_increment,
    group_id int not null,
    translator_id int,
    language_id int,
    capacity int not null default 1,
    foreign key (translator_id) references translators (id) on update cascade on delete cascade,
    foreign key (language_id) references languages (id) on update cascade on delete set null
);

create table currency_units (
    id int primary key auto_increment,
    name varchar(20) not null
);

create table price_list (
    id int primary key auto_increment,
    amount int default 0,
    unit_id int,
    foreign key (unit_id) references currency_units (id) on update cascade on delete set null
);

create table authors (
    id int primary key auto_increment,
    first_name varchar(40) default null,
    last_name varchar(40) not null,
    email varchar(60) default null,
    url varchar(1000) default null
);

create table authors_group (
    id int primary key auto_increment,
    group_id int not null,
    author_id int,
    capacity int not null default 1,
    foreign key (author_id) references authors (id) on update cascade on delete cascade
);

create table publishers (
    id int primary key auto_increment,
    name varchar(40) not null,
    country varchar(40) default null,
    email varchar(60) default null,
    phone varchar(20) default null,
    url varchar(1000) default null
);

create table books (
    id int primary key auto_increment,
    title varchar(60) not null,
    url varchar(1000) default null,
    cover varchar(1000) default null,
    publisher_id int,
    isbn_10 char(10) not null unique,
    isbn_13 char(13) not null unique,
    publish_date date default null,
    edition smallint not null default 1,
    volumes smallint default 1,
    authors_group_id int,
    price_id int,
    country varchar(20) default null,
    number_of_pages int not null default 0,
    translators_group_id int,
    language_id int,
    topic_id int default 1,
    foreign key (publisher_id) references publishers (id) on update cascade on delete set null,
    foreign key (authors_group_id) references authors_group (id) on update cascade on delete set null,
    foreign key (price_id) references price_list (id) on update cascade on delete set null,
    foreign key (translators_group_id) references translators_group (id) on update cascade on delete set null,
    foreign key (language_id) references languages (id) on update cascade on delete set null,
    foreign key (topic_id) references topics (id) on update cascade on delete set null
);

create table users (
    id int primary key auto_increment,
    first_name varchar(40) not null,
    last_name varchar(40) not null
);

create table states (
    id int primary key auto_increment,
    name varchar(40)
);

create table activities (
    id int primary key auto_increment,
    book_id int,
    shelf_id int,
    volume smallint not null default 1,
    user_id int default 0,
    has_borrowed boolean default false,
    has_lent boolean default false,
    insertion_date date default null,
    state_id int,
    rate int default 0,
    foreign key (book_id) references books (id) on update cascade on delete cascade,
    foreign key (shelf_id) references shelves (id) on update cascade on delete set null,
    foreign key (user_id) references users (id) on update cascade on delete cascade,
    foreign key (state_id) references states (id) on update cascade on delete set null
);
