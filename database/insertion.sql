insert into languages (name) values ('English');
insert into languages (name) values ('Persian');
insert into languages (name) values ('Arabic');
insert into languages (name) values ('German');
insert into languages (name) values ('Spanish');

insert into currency_units (name) values ('Rials');
insert into currency_units (name) values ('Dollars');
insert into currency_units (name) values ('Euros');

insert into topics (name) values ('Computer Science');
insert into topics (name) values ('Linguistics');
insert into topics (name) values ('Psychology');
insert into topics (name) values ('History');
insert into topics (name) values ('Religious');
insert into topics (name) values ('Romans');
insert into topics (name) values ('Arts');

insert into shelves (row_number,column_number,topic_id) values (1,1,null);
insert into shelves (row_number,column_number,topic_id) values (1,2,null);
insert into shelves (row_number,column_number,topic_id) values (1,3,null);
insert into shelves (row_number,column_number,topic_id) values (1,4,null);
insert into shelves (row_number,column_number,topic_id) values (1,5,null);
insert into shelves (row_number,column_number,topic_id) values (1,6,null);
insert into shelves (row_number,column_number,topic_id) values (2,1,null);
insert into shelves (row_number,column_number,topic_id) values (2,2,null);
insert into shelves (row_number,column_number,topic_id) values (2,3,null);
insert into shelves (row_number,column_number,topic_id) values (2,4,null);
insert into shelves (row_number,column_number,topic_id) values (2,5,null);
insert into shelves (row_number,column_number,topic_id) values (2,6,null);
insert into shelves (row_number,column_number,topic_id) values (3,1,null);
insert into shelves (row_number,column_number,topic_id) values (3,2,null);
insert into shelves (row_number,column_number,topic_id) values (3,3,null);
insert into shelves (row_number,column_number,topic_id) values (3,4,null);
insert into shelves (row_number,column_number,topic_id) values (3,5,null);
insert into shelves (row_number,column_number,topic_id) values (3,6,null);
insert into shelves (row_number,column_number,topic_id) values (4,1,null);
insert into shelves (row_number,column_number,topic_id) values (4,2,null);
insert into shelves (row_number,column_number,topic_id) values (4,3,null);
insert into shelves (row_number,column_number,topic_id) values (4,4,null);
insert into shelves (row_number,column_number,topic_id) values (4,5,null);
insert into shelves (row_number,column_number,topic_id) values (4,6,null);
insert into shelves (row_number,column_number,topic_id) values (5,1,null);
insert into shelves (row_number,column_number,topic_id) values (5,6,null);
insert into shelves (row_number,column_number,topic_id) values (6,1,null);
insert into shelves (row_number,column_number,topic_id) values (6,6,null);
insert into shelves (row_number,column_number,topic_id) values (7,1,null);
insert into shelves (row_number,column_number,topic_id) values (7,6,null);
insert into shelves (row_number,column_number,topic_id) values (8,1,null);

-- ordering matters
insert into states (name) values ("writing");
insert into states (name) values ("currently reading");
insert into states (name) values ("left unfinished");
insert into states (name) values ("want to read");
insert into states (name) values ("read");
