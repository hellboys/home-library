delimiter //

drop function if exists estimate_shelf_topic//

create function estimate_shelf_topic (
    shelf_id int
)
returns int
begin
    declare most_frequent_topic_id int default 0;

    select b.topic_id into most_frequent_topic_id
    from books b
    inner join activities a on a.book_id = b.id
    where a.shelf_id = shelf_id
    group by b.topic_id
    order by count(b.id) desc
    limit 1;

    return most_frequent_topic_id;
end//

delimiter ;
