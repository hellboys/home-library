delimiter //
drop procedure if exists move_book//

create procedure move_book (
    book_id int,
    new_shelf_id int,
    volume smallint -- optional
)
begin
    declare sql_exception bool default false;
    declare old_shelf_id int;
    declare book_volumes smallint default 1;
    declare continue handler for sqlexception set sql_exception = false;

    start transaction;

    if ifnull(volume, 0) = 0 then
        select b.volumes into book_volumes
        from books b
        where b.id = book_id;
    end if;

    /*
        if only a volume of a book is specified, shelf's number_of_holding_books will be decreased by 1,
        but if no volume number is specified, it means all volumes of a book, if there is more than one, is moved to another shelf
        so current shelf's holding_number_of_books will be decreased by the number of all volumes
    */
    update shelves
    set number_of_holding_books = number_of_holding_books - if(ifnull(volume,0) = 0, book_volumes, 1)
    where id in (
        select a.shelf_id from activities a
        where a.book_id = book_id and a.volume = ifnull(volume, a.volume)
        group by a.shelf_id
    );

    /*
        if only a volume of a book is specified, new shelf's number_of_holding_books will be increased by 1,
        but if no volume is specified, which means all volumes of a book, if there is more than one, is moved to this shelf
        so new shelf's number_of_holding_books will be increased by the number of all volumes
    */
    update shelves s
    set s.number_of_holding_books = s.number_of_holding_books + book_volumes
    where s.id = new_shelf_id;

    -- move book(s) to new shelf
    update activities a
    set a.shelf_id = new_shelf_id
    where a.book_id = book_id
    and a.volume = ifnull(volume, a.volume);

    -- update shelf's topic and capacity
    update shelves s
    set s.topic_id = estimate_shelf_topic(new_shelf_id),
        s.capacity = estimate_shelf_capacity(new_shelf_id)
    where s.id = new_shelf_id;

    if sql_exception = false then
        commit;
    else
        rollback;
    end if;
end//

delimiter ;
