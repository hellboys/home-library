delimiter //
drop function if exists suggest_shelf//

create function suggest_shelf (
    user_id int,
    topic_id int,
    number_of_pages int
)
returns int
begin
    declare suggested_shelf_id int default null;

    select s.id into suggested_shelf_id
    from books b
    inner join activities a on a.book_id = b.id
    inner join shelves s on s.id = a.shelf_id
    where a.user_id = user_id or s.number_of_holding_books < s.capacity or s.topic_id = topic_id
    group by s.usage_frequency, a.user_id, s.id, s.topic_id
    order by s.usage_frequency
    limit 1;

    if suggested_shelf_id is null then
        select id into suggested_shelf_id from shelves
        where number_of_holding_books < capacity
        group by number_of_holding_books, id
        order by number_of_holding_books desc, id
        limit 1;
    end if;

    return suggested_shelf_id;
end//
delimiter ;
