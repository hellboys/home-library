delimiter //
drop procedure if exists insert_book//

create procedure insert_book (
    isbn_13 char(13),
    isbn_10 char(10),
    title varchar(60),
    url varchar(1000),
    cover varchar(1000),
    publish_date date,
    edition smallint,
    volumes smallint,
    publisher_id int,
    publisher_name varchar(40),
    authors_group_id int,
    author_firstname varchar(40),
    author_lastname varchar(40),
    price int,
    currency_unit_id int,
    currency_unit_name varchar(20),
    shelf_row_number int,
    shelf_column_number int,
    country varchar(20),
    number_of_pages int,
    translators_group_id int,
    translator_firstname varchar(40),
    translator_lastname varchar(40),
    language_id int,
    language_name int,
    topic_id int,
    topic_name varchar(40),
    user_id int,
    user_firstname varchar(40),
    user_lastname varchar(40),
    has_borrowed boolean,
    has_lent boolean,
    state_id int,
    rate int
)
begin
    declare sql_exception boolean default false;
    declare book_id int;
    declare price_id int;
    declare author_id int default 0;
    declare new_authors_group_id int default 0;
    declare translator_id int default 0;
    declare capacity int default 5;
    declare shelf_id int default null;
    declare new_translators_group_id int default 0;
    declare suggested_shelf_id int default null;
    declare most_frequent_topic_id int default 0;
    declare volume smallint default 1;

    declare continue handler for sqlexception set sql_exception = true;

    start transaction;

    -- if new publisher is specified, create one
    if ifnull(publisher_id, 0) = 0 and publisher_name is not null then
        insert into publishers (name) values (publisher_name);
        set publisher_id = last_insert_id();
    elseif ifnull(publisher_id, 0) = 0 then
        select 'publisher cannot be left unset' as error;
    end if;

    -- if new author is specified, create one
    if ifnull(authors_group_id, 0) = 0 and author_lastname is not null then
        insert into authors (first_name, last_name) values (author_firstname, author_lastname);
        set author_id = last_insert_id();
        select ifnull(max(group_id) + 1, 1) into new_authors_group_id from authors_group;
        insert into authors_group (group_id, author_id) values (new_authors_group_id, author_id);
        set authors_group_id = last_insert_id();
    elseif ifnull(authors_group_id, 0) = 0 then
        select 'author cannot be left unset' as error;
    end if;

    -- if new currency unit specified, create one
    if ifnull(currency_unit_id, 0) = 0 and currency_unit_name is not null then
        insert into units (name) values (currency_unit_name);
        set currency_unit_id = last_insert_id();
    end if;

    -- insert price
    insert into price_list (amount, unit_id) values (ifnull(price, 0), currency_unit_id);
    set price_id = last_insert_id();

    -- if new user specified, create one
    if ifnull(user_id, 0) = 0 and user_firstname is not null and user_lastname is not null then
        insert into users (first_name, last_name) values (user_firstname, user_lastname);
        set user_id = last_insert_id();
    elseif ifnull(user_id, 0) = 0 then
        select 'user cannot be left unspecified' as error;
    end if;

    -- prepare a shelf
    if shelf_row_number is not null and shelf_column_number is not null then
        -- get shelf id if coordinates are specified
        select id into shelf_id from shelves
        where row_number = shelf_row_number and column_number = shelf_column_number;

        update shelves set number_of_holding_books = number_of_holding_books + volumes
        where id = shelf_id;

        -- new topic will be created if specified
        if ifnull(topic_id,0) = 0 and topic_name is not null then
            insert into topics (name) values (topic_name);
            set topic_id = last_insert_id();
        elseif ifnull(topic_id,0) = 0 then
            select 'topic cannot be left blank' as error;
        end if;

        -- update shelf capacity
        set capacity = estimate_shelf_capacity(shelf_id);
        update shelves set capacity = capacity where id = shelf_id;
    else
        set shelf_id = suggest_shelf(user_id, topic_id, number_of_pages);
    end if;

    -- if new language specified, create one
    if ifnull(language_id, 0) = 0 and language_name is not null then
        insert into languages (name) values (language_name);
        set language_id = last_insert_id();
    end if;

    if ifnull(translators_group_id, 3) = 0 and translator_lastname is not null then
        insert into translators (first_name, last_name) values (translator_firstname, translator_lastname);
        set translator_id = last_insert_id();
        select ifnull(max(id) + 1, 1) into new_translators_group_id from translators_group;
        insert into translators_group (group_id, translator_id, language_id) values (new_translators_group_id, translator_id, language_id);
    end if;

    -- insert book
    if title is null then
        select 'title cannot be left blank' as error;
    end if;

    insert into books (
        title, url, cover, publisher_id, isbn_10, isbn_13, publish_date,
        edition, volumes, authors_group_id, price_id, country,
        number_of_pages, translators_group_id, language_id, topic_id
    ) values (
        title, url, cover, publisher_id, isbn_10, isbn_13, publish_date,
        edition, volumes, authors_group_id, price_id, country,
        number_of_pages, translators_group_id, language_id, topic_id
    );

    set book_id = last_insert_id();
    if state_id != 3 then
        set rate = null;
    end if;

    while volume <= volumes do
        insert into activities (book_id, shelf_id, volume, user_id, has_borrowed, has_lent, insertion_date, state_id, rate)
        values (book_id, shelf_id, volume, user_id, has_borrowed, has_lent, curdate(), state_id, rate);

        set volume = volume + 1;
    end while;

    set most_frequent_topic_id = estimate_shelf_topic(shelf_id);
    update shelves set topic_id = most_frequent_topic_id where id = shelf_id;

    if sql_exception = false then
        commit;
    else
        rollback;
    end if;
end//

delimiter ;
