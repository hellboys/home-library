delimiter //
drop function if exists estimate_shelf_capacity //

create function estimate_shelf_capacity (
    shelf_id int
)
returns int
begin
    declare shelf_capacity int default 0;
    declare number_of_books int default 0;
    declare shelf_contained_pages int default 0;
    declare max_contained_pages int default 0;
    declare most_populated_shelf_id int default 0;
    declare mean_book_size_in_shelf int default 0;
    declare estimated_capacity int default 5;

    -- chosen shelf info
    select id, number_of_holding_books, capacity into shelf_id, shelf_capacity, number_of_books
    from shelves
    where id = shelf_id;

    -- check if shelf does really exist in the library
    if ifnull(shelf_id,0) = 0 then
        signal sqlstate '10000' set message_text = 'shelf does not exist';
    end if;

    -- number of pages that chosen shelf is contained of
    select sum(b.number_of_pages) into shelf_contained_pages
    from books b
    inner join activities a on a.book_id = b.id
    where a.shelf_id = shelf_id and a.volume = 1;

    -- maximum number of pages held in neighbourhood
    select sum(b.number_of_pages) into max_contained_pages
    from books b
    inner join activities a on a.book_id = b.id
    where a.shelf_id = most_populated_shelf_id
    group by b.id;

    -- in case the chosen shelf does not have neighbours vertically
    if most_populated_shelf_id = 0 then
        set estimated_capacity = shelf_capacity;
    else
        set mean_book_size_in_shelf = shelf_contained_pages / number_of_books;

        if shelf_contained_pages < max_contained_pages then
            set estimated_capacity = max_contained_pages / mean_book_size_in_shelf;
        else
            set estimated_capacity = number_of_books;
        end if;
    end if;

    return estimated_capacity;
end//
delimiter ;
