#!/bin/bash

mysql -u brian -plinuxlab < database.sql
mysql -u brian -plinuxlab library < insertion.sql
mysql -u brian -plinuxlab library < components/estimate_shelf_capacity.sql
mysql -u brian -plinuxlab library < components/estimate_shelf_topic.sql
mysql -u brian -plinuxlab library < components/suggest_shelf.sql
mysql -u brian -plinuxlab library < components/insert_book.sql
mysql -u brian -plinuxlab library < samples.sql
