#ifndef CELLS_
#define CELLS_

#include <vector>

const int HIGH = 1;
const int LOW = 0;

void pinMode(pin_t controller, int state) {
    std::cout << "pin " << controller << " is " << std::boolalpha << state << std::endl;
}

void digitalWrite(pin_t controller, int state) {
    std::cout << "writing into digital pin " << controller << ": " << std::boolalpha << state << std::endl;
}

void analogWrite(pin_t controller, int state) {
    std::cout << "writing into analog pin " << controller << ": " << std::boolalpha << state << std::endl;
}

void delay(const int ms) {
    std::cout << "waiting for " << ms << " ms" << std::endl;
}


#define A0 8
#define A1 9
#define A2 10
#define A3 11
#define A4 12
#define A5 13

typedef unsigned int pin_t;

Library::Library() {
    library_size = rows * columns;
    create_imaginary_shelves();
}

Library::Library(const pin_t row, const pin_t column) {
    library_size = rows * columns;
    create_imaginary_shelves();
}

void Library::setup() {
    std::vector<pin_t> temp = {2,3,4,5,6,7,A1,A2,A0,A3,A4,A5};
    shelves = temp;
}

void Library::initialize() const {
    for_each (shelves.cbegin(), shelves.cend(), [](const pin_t controller) {
        pinMode(controller, OUTPUT);
    }
}

void Library::read(const int data) const {
    switch(data) {
    case 'w':
        wipe_shelves();
        break;
    case 's':
        search_mode = true;
        break;
    case 'f':
        found_shell(5);
    case '0':
        incolumn_periodic_tail();
        dim_all_shelves();
        blink_mode = false;
        blinker = 0;
        fade_ratio = -5;
        brightness = 255;
        break;
    case '1':
        inorder_periodic_tail(); // 1
        break;
    case 'e':
        inorder_periodic_tail(cell); // 1 exception
        break;
    case '2':
        inorder_periodic_load(); // 2
        break;
    case '3':
        inorder_permanent_load(); // 3
        break;
    case '4':
        incolumn_periodic_tail(); // 4
        break;
    case 'E':
        incolumn_periodic_tail(cell); // 4 exception
        break;
    case '5':
        incolumn_periodic_load(); // 5
        break;
    case '6':
        incolumn_permanent_load(); // 6
        break;
    case '7':
        indiagonal_periodic_tail(); // 7
        break;
    case '8':
        light_all_shelves(); // 8
        blink_mode = true;
        break;
    case '9':
        dim_all_shelves(); // 9
        blink_mode = true;
        break;
    case 'A':
        incolumn_periodic_pendulum(); // A
        break;
    case 'B':
        blink_mode = true; // B
        break;
    case 'C':
        blink_found_cell(2); // C
        break;
    case 'D':
        indiagonal_periodic_tail(6); // D
        break;
    default:
        break;
    }

}

void Library::reset_to_defaults() const {
    blink_mode = false;
    brightness = 0;
    fade_ratio = 5;
    blinker = 0;
}

void Library::light_all_shelves(const pin_t exception = 0) const {
    for_each (shelves.cbegin(), shelves.cend(), [](const int& shelf) {
        if (shelf != exception) {
            digitalWrite(shelves[shelf], HIGH);
        }
    }
}

void Library::dim_all_shelves(const pin_t exception = 0) const {
    reset_to_defaults();
    for_each (shelves.cbegin(), shelves.cend(), [](const pin_t& shelf) {
        if (shelf != exception) {
            digitalWrite(shelves[shelf], LOW);
        }
    }
}

void Library::light_shelves_in_row(const pin_t first_shelf, const pin_t length, const pin_t exception = 0) const {
    std::vector<pin_t>::iterator shelf_iterator = shelves.begin() + first_shelf;
    for_each (shelf_iterator, shelf_iterator + length, [](const pin_t shelf) {
        pin_t controller = shelves[shelf];
        if (controller > 0 && shelf != exception) {
            digitalWrite(controller, HIGH);
        }
    }
}

void Library::light_column(const pin_t column, const pin_t exception = 0) const {
    std::vector<pin_t>::iterator shelf;
    for (shelf = shelves.begin(); shelf != shelves.end(); shelf += columns) {
        pin_t controller = shelves[*shelf];
        if (controller > 0 && *shelf != exception) {
            digitalWrite(controller, HIGH);
        }
    }
}

void Library::dim_column(const pin_t column, const pin_t exception = 0) const {
    reset_to_default();
    std::vector<pin_t>::iterator shelf;
    for (shelf = shelves.begin(); shelf != shelves.end(); shelf += columns) {
        pin_t controller = shelves[*shelf];
        if (controller > 0 && *shelf != exception) {
            digitalWrite(controller, LOW);
        }
    }
}

void Library::create_imaginary_shelves() const {
    pin_t longest = (columns >= rows) ? columns : rows;
    pin_t shortest = (columns >= rows) ? rows : columns;
    imaginary_columns = longest + (shortest-1);
    imaginary_rows = rows;

    for (pin_t row = 0; row < imaginary_rows; ++row) {
        for (pin_t column = 0; column < imaginary_columns; ++column) {
            if (column >= columns || row >= rows) {
                imaginary_shelves.push(0);
            }
            else {
                pin_t shelf = column + (row * columns);
                imaginary_shelves.push(shelves[shelf]);
            }
        }
    }
}

void Library::light_diagonal(const pin_t shelf, const pin_t exception = 0) const {
    for (pin_t row = 0, pin_t column = shelf;
        row < imaginary_rows && column < imaginary_columns;
        --columns, ++row) {
        pin_t next_shelf = column + (row * imaginary_columns);
        pin_t controller = imaginary_shelves[next_shelf];
        if (controller > 0 && next_shelf != exception) {
            digitalWrite(controller, HIGH);
        }
    }
}

void Library::wipe_shelves(const pin_t exception = 0) const {
    for (pin_t counter = 0; counter < columns; ++counter) {
        light_column(counter);
        delay(80);
        dim_column(counter, exception);
    }
}

void Library::search_shelves(const pin_t exception = 0) const {
    pin_t imaginary_library_size = imaginary_rows * imaginary_columns;

    for (pin_t column = 0; column < imaginary_columns; ++column) {
        light_diagonal(column);
        delay(80);
        dim_all_shelves(exception);
    }
}

void Library::found_shell(const pin_t shelf) const {
    search_mode = false;
    blink_mode = true;
    blinker = shelf;
}

void Library::blink_shelf(const pin_t shelf) const {
    analogWrite(shelf-1, brightness);
    brightness += fade_ratio;
    if (brightness <= 0 || brightness >= 255) {
        fade_ratio = -fade_ratio;
    }
    delay(10);
}

#endif
