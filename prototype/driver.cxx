#include <iostream>
#include <vector>
#include "library.hpp"

int main()
{
    Library library(3,4);
    library.setup();
    library.initialize();
    library.light_all_shelves();
    delay(1000);
    library.dim_all_shelves();
    library.search_shelves();
    
    return 0;
}
