#include <SoftwareSerial.h>
#include "library.cxx"

SoftwareSerial BT(10, 11); // RX, TX
int bluetooth_received;

pin_t blinker = 0;
const int button = 13;
int state = 0;

bool blink_mode = false;
bool search_mode = false;

void setup() {
    BT.begin(9600);

    Library library(3,4);
    library.setup();
    library.initialize();
    library.light_all_shelves();
    delay(1000);
    library.dim_all_shelves();
}

void loop() {
    if (BT.available()){
        library.read(BT.read());
    }
    BT.flush();

    if (search_mode == true) {
        library.search_shelves();
    }
    else if (blink_mode == true) {
        library.blink_shelf(blinker);
    }


  bluetooth_received = 0;
}
