#include "library.hpp"

class Library {
public:
    Library(): rows{3}, columns{4} {};
    Library(const pin_t row, const pin_t column): rows{row}, columns{column} {};
    ~Library() {};

    // function members
    inline void reset_to_defaults() const;
    void light_all_shelves(const pin_t exception = 0) const;
    void dim_all_shelves(const pin_t exception = 0) const;
    void light_shelves_in_row(const pin_t first_shelf, const pin_t length, const pin_t exception = 0) const;
    void light_column(const pin_t column, const pin_t exception = 0) const;
    void dim_column(const pin_t column, const pin_t exception = 0) const;
    void light_diagonal(const pin_t shelf, const pin_t exception = 0) const;
    void wipe_shelves(const pin_t exception = 0) const;
    void search_shelves(const pin_t exception = 0) const;
    void found_shelf(const pin_t shelf) const;
    void blink_shelf(const pin_t shelf) const;
    void read() const;
    void setup();
    void initialize() const;
    void create_imaginary_shelves() const;

private:
    // members
    std::vector<pin_t> shelves;
    pin_t rows;
    pin_t columns;
    pin_t library_size;

    std::vector<pin_t> imaginary_shelves;
    pin_t imaginary_rows;
    pin_t imaginary_columns;

    int brightness = 0;
    int fade_ratio = 5;
};
